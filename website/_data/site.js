module.exports = {
  baseUrl:
    process.env.ELEVENTY_ENV === "development"
      ? "http://localhost:8080"
      : "https://calvinguillot.gitlab.io/digital-fabrication",
  title: "Digital Fabrication",
  subTitle: "by Calvin Guillot",
};
