<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" href="/css/style.css"/>
    
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Josefin+Sans:wght@300;700&family=PT+Serif&display=swap" rel="stylesheet">
    <title>Digital Fabrication</title>
  </head>
  <body>
    <header><div class="menu">
  <div class="menu-item">
    <a href=/>Home</a>
  </div>
  <div class="menu-item">
    <a href=/assignments>Assignments</a>
  </div>
  <div class="menu-item">
    <a href=/project>Project</a>
  </div>
  <div class="menu-item">
    <a href=/resources>Resources</a>
  </div>
  <div class="menu-item">
    <a href=/about>About</a>
  </div>
</div></header>
    <main>
      
<div class="page">
  <div class="page-title">Resources</div>
  <div class="page-content">
    
<div id="resourcesList" class="list-page">
  <script src="permalink"></script>
</div>


</div>
</div>
    </main>
  </body>
</html>